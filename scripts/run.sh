#!/bin/bash
useradd -m -p vnc -s /bin/bash vnc
mkdir /home/vnc/.config
chown vnc:vnc /home/vnc/.config
mkdir /home/vnc/.config/autostart
chown vnc:vnc /home/vnc/.config/autostart
mv /root/firefox.desktop /home/vnc/.config/autostart/
chown vnc:vnc /home/vnc/.config/autostart/firefox.desktop
gpasswd -a vnc sudo
chown vnc:vnc /vncsetup.exp
su vnc -c "./vncsetup.exp"
mv /root/scripts/xstartup /home/vnc/.vnc/xstartup
#echo "/usr/bin/firefox" >> /home/vnc/.vnc/xstartup
chown vnc:vnc /home/vnc/.vnc/xstartup
mv /root/xfce4.cfg.tar.gz/xfce4 /home/vnc/.config/
#tar -xzvf /root/xfce4.cfg.tar.gz -C /home/vnc/.config/
rm -R /root/xfce4.cfg.tar.gz
chown -Rv vnc:vnc /home/vnc/.config/xfce4
mkdir /home/vnc/.config/icedtea-web
mv /root/deployment.properties /home/vnc/.config/icedtea-web
chown -Rv vnc:vnc /home/vnc/.config/icedtea-web
mv /root/scripts/ProRealInitializer/ProRealInitializer /home/vnc/
chown -Rv vnc:vnc /home/vnc/ProRealInitializer

#Install python pack
#pip3 install sshtunnel

#Configure and run dbus daemon
dbus-uuidgen > /var/lib/dbus/machine-id
mkdir -p /var/run/dbus
dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address


su vnc -c "chmod u+x /home/vnc/.vnc/xstartup"
su vnc -c "vncserver -geometry 3840x2160"
bash