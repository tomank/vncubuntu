#!/bin/bash
sudo apt-get update
sudo apt-get -y install apt-utils curl
sudo apt-get -y install python3-distutils
sudo apt-get -y install python3-setuptools
sudo apt-get -y install python3-pip
sudo apt-get -y install python3-tk python3-dev

sudo apt-get update
sudo apt-get install -y unzip xvfb libxi6 libgconf-2-4
sudo apt-get install -y default-jdk
sudo curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add
sudo echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
sudo apt-get -y install chromium-chromedriver
sudo apt-get update
sudo apt-get -y install google-chrome-stable
#wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
mkdir /root/deb/chromedriver_64
unzip /root/deb/chromedriver_linux64.zip -d /root/deb/chromedriver_64
sudo mv /root/deb/chromedriver_64/chromedriver /usr/bin/chromedriver
sudo chown root:root /usr/bin/chromedriver
sudo chmod +x /usr/bin/chromedriver
#wget https://selenium-release.storage.googleapis.com/3.13/selenium-server-standalone-3.13.0.jar
#wget http://www.java2s.com/Code/JarDownload/testng/testng-6.8.7.jar.zip
#unzip testng-6.8.7.jar.zip
sudo apt-get -y install xsel

sudo apt-get -y install xdpyinfo
sudo apt-get -y install wmctrl
sudo apt-get -y install xdotool



