FROM ubuntu
MAINTAINER Tomasz Ankowski

#Install expect
RUN apt update
RUN apt -y install expect

#Make order catalogs
RUN mkdir /root/scripts
RUN mkdir /root/deb

#Copy scripts, program and config files
ADD scripts/install-mc.exp /root/scripts/install-mc.exp
ADD scripts/install-mc.sh /root/scripts/install-mc.sh
ADD scripts/install-x11.exp /root/scripts/install-x11.exp
ADD scripts/apti.sh /root/scripts/apti.sh
ADD scripts/run.sh /run.sh
ADD scripts/vncsetup.exp /vncsetup.exp
ADD deb/firefox-45.0.2.deb /root/deb/firefox-45.0.3.deb
ADD deb/chromedriver_linux64.zip /root/deb/chromedriver_linux64.zip
ADD config/firefox.desktop /root/firefox.desktop
ADD scripts/xstartup /root/scripts/xstartup
ADD config/deployment.properties /root/deployment.properties
ADD scripts/msfonts.sh /root/scripts/msfonts.sh
ADD scripts/msfonts.exp /root/scripts/msfonts.exp
ADD scripts/pri-install.sh /root/scripts/pri-install.sh
ADD scripts/ProRealInitializer /root/scripts/ProRealInitializer

#Set permissions
RUN chmod u+x /root/scripts/*
RUN chmod u+x /run.sh
RUN chmod u+x /vncsetup.exp

#Run config scripts
RUN expect /root/scripts/install-mc.exp
RUN expect /root/scripts/install-x11.exp

#Set xfce4 settings
ADD config/xfce4.cfg.tar.gz /root/xfce4.cfg.tar.gz

#Install VNC Server
RUN apt -y install tightvncserver sudo

#Instal Web Browser
RUN dpkg -i --force-all /root/deb/firefox-45.0.3.deb
RUN apt -y install -f

#Install java
RUN apt -y install openjdk-8-jdk

#Install fonts
RUN expect /root/scripts/msfonts.exp

#Install PRI
RUN ./root/scripts/pri-install.sh

#Set autorun script
CMD ["./run.sh"]
